# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_27_125924) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.integer "role_id"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_accounts_on_confirmation_token", unique: true
    t.index ["email"], name: "index_accounts_on_email", unique: true
    t.index ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_accounts_on_unlock_token", unique: true
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "name"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "balance"
  end

  create_table "budget_expenditures", force: :cascade do |t|
    t.integer "budget_id"
    t.integer "accounting_code_id"
    t.string "accounting_code_type"
    t.string "actual"
    t.string "estimated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "budget_revenues", force: :cascade do |t|
    t.integer "budget_id"
    t.integer "accounting_code_id"
    t.string "accounting_code_type"
    t.string "actual"
    t.string "estimated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.integer "financial_year_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "code_categories", force: :cascade do |t|
    t.string "number"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "codes", force: :cascade do |t|
    t.string "number"
    t.string "name"
    t.integer "code_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_years", force: :cascade do |t|
    t.string "name"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "is_current"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "payment_vouchers", force: :cascade do |t|
    t.string "number"
    t.integer "financial_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "petty_cash", force: :cascade do |t|
    t.string "amount"
    t.string "amount_in_words"
    t.integer "financial_year_id"
    t.integer "payment_voucher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "petty_cash_vouchers", force: :cascade do |t|
    t.string "number"
    t.integer "financial_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "petty_cash_id"
  end

  create_table "revenue_divisions", force: :cascade do |t|
    t.json "percentage_divisions"
    t.integer "accounting_code_id"
    t.string "accounting_code_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "revenues", force: :cascade do |t|
    t.integer "financial_year_id"
    t.string "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "money_divisions"
    t.integer "accounting_code_id"
    t.string "accounting_code_type"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "permissions"
  end

  create_table "sub_codes", force: :cascade do |t|
    t.string "name"
    t.string "number"
    t.integer "code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "system_logs", force: :cascade do |t|
    t.integer "account_id"
    t.string "action"
    t.datetime "action_time"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "loggable_id"
    t.string "loggable_type"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "vouchers", force: :cascade do |t|
    t.string "payee"
    t.string "amount"
    t.string "amount_in_words"
    t.text "details"
    t.integer "created_by"
    t.integer "approved_by"
    t.integer "currency"
    t.string "payment_type"
    t.integer "payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "bank_account_id"
    t.string "accounting_code_type"
    t.json "code_amount_divisions"
    t.integer "authorized_by"
    t.string "cheque_no"
    t.datetime "approval_date"
    t.datetime "authorization_date"
  end

end
