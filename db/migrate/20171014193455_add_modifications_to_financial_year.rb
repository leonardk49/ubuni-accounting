class AddModificationsToFinancialYear < ActiveRecord::Migration[5.1]
  def change
    change_column_default :financial_years, :is_current, from: nil, to: false
  end
end
