class RenamePaymentTypeId < ActiveRecord::Migration[5.1]
  def change
    rename_column :vouchers, :payment_type_id, :payment_id
  end
end
