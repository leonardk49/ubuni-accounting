class AddUnconfirmedEmailToAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :unconfirmed_email, :string
  end
end
