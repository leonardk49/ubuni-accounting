class AddPermissionsToRole < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :permissions, :json
  end
end
