class ModifyCodesInVouchers < ActiveRecord::Migration[5.1]
  def change
    remove_column :vouchers, :code_id, :integer
    remove_column :vouchers, :code_type, :string
    add_column :vouchers, :accounting_code_type, :string
    add_column :vouchers, :code_amount_divisions, :json
  end
end
