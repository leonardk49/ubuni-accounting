class AddBankIdToVoucher < ActiveRecord::Migration[5.1]
  def change
    add_column :vouchers, :bank_id, :integer
  end
end
