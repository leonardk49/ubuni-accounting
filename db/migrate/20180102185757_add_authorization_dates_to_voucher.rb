class AddAuthorizationDatesToVoucher < ActiveRecord::Migration[5.1]
  def change
    add_column :vouchers, :approval_date, :datetime
    add_column :vouchers, :authorization_date, :datetime
  end
end
