class CreateBankAccountRevenueTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_account_revenue_types do |t|
      t.integer :bank_account_id
      t.integer :revenue_type_id
      t.string :percentage

      t.timestamps
    end
  end
end
