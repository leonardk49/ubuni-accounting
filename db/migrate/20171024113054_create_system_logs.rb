class CreateSystemLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :system_logs do |t|
      t.integer :account_id
      t.string :action
      t.datetime :action_time
      t.text :notes

      t.timestamps
    end
  end
end
