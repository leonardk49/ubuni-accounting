class CreatePaymentVouchers < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_vouchers do |t|
      t.string :number
      t.integer :financial_year_id

      t.timestamps
    end
  end
end
