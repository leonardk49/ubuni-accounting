class AddMoneyDivisionsToRevenue < ActiveRecord::Migration[5.1]
  def change
    add_column :revenues, :money_divisions, :json
  end
end
