class CreateSubCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :sub_codes do |t|
      t.string :name
      t.string :number
      t.integer :code_id

      t.timestamps
    end
  end
end
