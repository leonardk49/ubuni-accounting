class CreateRevenues < ActiveRecord::Migration[5.1]
  def change
    create_table :revenues do |t|
      t.integer :financial_year_id
      t.string :amount
      t.integer :revenue_type_id

      t.timestamps
    end
  end
end
