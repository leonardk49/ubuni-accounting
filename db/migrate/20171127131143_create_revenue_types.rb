class CreateRevenueTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :revenue_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
