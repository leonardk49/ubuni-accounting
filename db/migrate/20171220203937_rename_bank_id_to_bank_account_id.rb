class RenameBankIdToBankAccountId < ActiveRecord::Migration[5.1]
  def change
    rename_column :vouchers, :bank_id, :bank_account_id
  end
end
