class AddDeletedAtToFinancialYear < ActiveRecord::Migration[5.1]
  def change
    add_column :financial_years, :deleted_at, :datetime
  end
end
