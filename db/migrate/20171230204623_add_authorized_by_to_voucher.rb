class AddAuthorizedByToVoucher < ActiveRecord::Migration[5.1]
  def change
    add_column :vouchers, :authorized_by, :integer
  end
end
