class MakeSystemLogPolymorphic < ActiveRecord::Migration[5.1]
  def change
    add_column :system_logs, :loggable_id, :integer
    add_column :system_logs, :loggable_type, :string
  end
end
