class CreateBudgetExpenditures < ActiveRecord::Migration[5.1]
  def change
    create_table :budget_expenditures do |t|
      t.integer :budget_id
      t.integer :accounting_code
      t.string :accounting_code_type
      t.string :actual
      t.string :estimated

      t.timestamps
    end
  end
end
