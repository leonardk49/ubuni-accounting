class RemoveRevenueTypes < ActiveRecord::Migration[5.1]
  def change
    drop_table :revenue_types
    drop_table :bank_account_revenue_types
  end
end
