class UpdateRevenueModels < ActiveRecord::Migration[5.1]
  def change
    remove_column :revenues, :revenue_type_id, :integer
    remove_column :revenue_divisions, :bank_account_id, :integer
  end
end
