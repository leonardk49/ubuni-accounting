class AddAccountingCodeToRevenue < ActiveRecord::Migration[5.1]
  def change
    add_column :revenues, :accounting_code_id, :integer
    add_column :revenues, :accounting_code_type, :string
  end
end
