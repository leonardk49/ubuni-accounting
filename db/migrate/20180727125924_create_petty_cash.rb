class CreatePettyCash < ActiveRecord::Migration[5.2]
  def change
    create_table :petty_cash do |t|
      t.string :amount
      t.string :amount_in_words
      t.integer :financial_year_id
      t.integer :payment_voucher_id

      t.timestamps
    end
  end
end
