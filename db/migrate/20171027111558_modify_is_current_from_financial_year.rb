class ModifyIsCurrentFromFinancialYear < ActiveRecord::Migration[5.1]
  def change
    remove_column :financial_years, :is_current, :boolean
    add_column :financial_years, :is_current, :string
  end
end
