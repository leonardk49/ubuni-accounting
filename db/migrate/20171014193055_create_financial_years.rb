class CreateFinancialYears < ActiveRecord::Migration[5.1]
  def change
    create_table :financial_years do |t|
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_current

      t.timestamps
    end
  end
end
