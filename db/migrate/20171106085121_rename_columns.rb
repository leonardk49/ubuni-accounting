class RenameColumns < ActiveRecord::Migration[5.1]
  def change
    rename_column :budget_revenues, :accounting_code, :accounting_code_id
    rename_column :budget_expenditures, :accounting_code, :accounting_code_id
  end
end
