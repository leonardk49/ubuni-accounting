class AddPettyCashIdToPettyCashVoucher < ActiveRecord::Migration[5.2]
  def change
    add_column :petty_cash_vouchers, :petty_cash_id, :integer
  end
end
