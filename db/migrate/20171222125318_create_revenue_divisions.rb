class CreateRevenueDivisions < ActiveRecord::Migration[5.1]
  def change
    create_table :revenue_divisions do |t|
      t.json :percentage_divisions
      t.integer :bank_account_id
      t.integer :accounting_code_id
      t.string :accounting_code_type

      t.timestamps
    end
  end
end
