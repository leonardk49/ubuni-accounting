class CreateVouchers < ActiveRecord::Migration[5.1]
  def change
    create_table :vouchers do |t|
      t.string :payee
      t.string :amount
      t.string :amount_in_words
      t.text :details
      t.integer :code_id
      t.string :code_type
      t.integer :created_by
      t.integer :approved_by
      t.integer :currency
      t.string :payment_type
      t.integer :payment_type_id

      t.timestamps
    end
  end
end
