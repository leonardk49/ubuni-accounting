upstream puma_cct_accounting {
  server unix:///var/www/cct/web/shared/tmp/sockets/accounting-puma.sock;
}

server {
    listen 80;
    listen [::]:80;
    server_name accounting.cct-udsm.org www.accounting.cct-udsm.org;

    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name accounting.cct-udsm.org;

    return 301 https://www.$server_name$request_uri;

    ssl_certificate /etc/letsencrypt/live/accounting.cct-udsm.org/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/accounting.cct-udsm.org/privkey.pem; # managed by Certbot
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name www.accounting.cct-udsm.org;

  root /var/www/cct/accounting/current/public;
  access_log /var/www/cct/accounting/current/log/nginx.access.log;
  error_log /var/www/cct/accounting/current/log/nginx.error.log info;

  location ^~ /assets/ {
    gzip_static on;
    expires max;
    add_header Cache-Control public;
  }

  try_files $uri/index.html $uri @puma_cct_accounting;
  location @puma_cct_accounting {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;

    proxy_pass http://puma_cct_accounting;
  }

  error_page 500 502 503 504 /500.html;
  client_max_body_size 10M;
  keepalive_timeout 10;

  ssl_certificate /etc/letsencrypt/live/accounting.cct-udsm.org/fullchain.pem; # managed by Certbot
  ssl_certificate_key /etc/letsencrypt/live/accounting.cct-udsm.org/privkey.pem; # managed by Certbot
}
