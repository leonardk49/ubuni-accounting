Rails.application.routes.draw do
  devise_for :accounts, controllers: { sessions: "accounts/sessions",
    registrations: 'accounts/registrations' }

  match '/save_expenditure',  to: 'budgets#save_expenditure',  via: :post, as: :save_expenditure
  match '/save_revenue',  to: 'budgets#save_revenue',  via: :post, as: :save_revenue
  match '/approve_payment_voucher',  to: 'payment_vouchers#approve',  via: :post, as: :approve_payment_voucher
  match '/approve_petty_cash',  to: 'petty_cash_vouchers#approve',  via: :post, as: :approve_petty_cash
  match '/authorize_payment_voucher',  to: 'payment_vouchers#authorize',  via: :post, as: :authorize_payment_voucher
  match '/authorize_petty_cash',  to: 'petty_cash_vouchers#authorize',  via: :post, as: :authorize_petty_cash
  match '/accounting',  to: 'dashboard#index',  via: :get, as: :accounting_index

  root 'dashboard#index'
  resources :dashboard, only: [:index]
  resources :roles
  resources :financial_years do
    resources :budgets
    resources :revenues
    resources :petty_cash
    resources :petty_cash_vouchers
    resources :payment_vouchers
    resources :vouchers
  end
  resources :bank_accounts
  resources :code_categories do
    resources :codes do
      resources :sub_codes
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
