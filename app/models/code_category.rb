class CodeCategory < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  has_many :codes
  has_many :sub_codes, through: :codes

  def self.expenses
    where("lower(name) = ?", 'expenses').first
  end

  def self.revenue
    revenue_category = where("lower(name) = ?", 'revenue').first
  end

  def available_codes(budget,code_type)
    if code_type == 'revenue'
      non_available_code_ids = budget.budget_revenues.pluck(:accounting_code_id)
    else
      non_available_code_ids = budget.budget_expenditures.pluck(:accounting_code_id)
    end
    codes_available = codes.where.not(id: non_available_code_ids)
  end
end
