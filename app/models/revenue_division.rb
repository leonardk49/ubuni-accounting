class RevenueDivision < ApplicationRecord
  belongs_to :accounting_code, polymorphic: true
end
