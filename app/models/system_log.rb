class SystemLog < ApplicationRecord
  belongs_to :account
  belongs_to :loggable, polymorphic: true
end
