class PaymentVoucher < ApplicationRecord
  include SharedTasks
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :financial_year
  has_one :voucher, as: :payment, dependent: :destroy
  has_many :petty_cash
  # accepts_nested_attributes_for :voucher
  after_create :create_voucher_number

  private

  def create_voucher_number
    financial_year = self.financial_year
    voucher_number = "PV/#{financial_year.name}/#{add_zeros(self.id)}"
    self.update(number: voucher_number)
  end
end
