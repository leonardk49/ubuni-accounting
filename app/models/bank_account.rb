class BankAccount < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  has_many :bank_account_revenue_types
  has_many :vouchers

  def add_balance(increment)
    increment = BigDecimal.new(increment)
    bank_balance = balance
    bank_balance = 0.0 if bank_balance.nil?
    bank_balance = BigDecimal.new(bank_balance, 2)
    bank_balance += increment
    self.update(balance: bank_balance)
    puts "Debugging"
    puts "#{increment} - #{bank_balance}"
  end
end
