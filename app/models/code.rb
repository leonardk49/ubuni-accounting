class Code < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :code_category
  has_one :revenue_division, as: :accounting_code, dependent: :destroy
  has_many :sub_codes
  has_many :budget_revenues, as: :accounting_code, dependent: :delete_all
  has_many :budget_expenditures, as: :accounting_code, dependent: :delete_all
  has_many :revenues, as: :accounting_code, dependent: :delete_all

  def save_revenue_divisions(revenue_division_param)
    percentage_divisions = {}
    BankAccount.all.each do |bank_account|
      revenue_division = revenue_division_param[bank_account.name.gsub(" ","_")]
      revenue_division = "0" if revenue_division.blank?
      percentage_divisions[bank_account.id.to_s] = revenue_division
    end
    RevenueDivision.create(accounting_code_id: self.id, accounting_code_type: self.class.to_s,
      percentage_divisions: percentage_divisions)
  end

  def percentage_divisions
    str = []
    items = self.revenue_division.percentage_divisions
    items.each do |key,value|
      str  << value
    end
    return str.join(',')
  end

  def bank_ids
    str = []
    items = self.revenue_division.percentage_divisions
    items.each do |key,value|
      str  << key
    end
    return str.join(',')
  end

  def self.revenue_codes
    revenue_code_category = CodeCategory.revenue
    if revenue_code_category
      revenue_code_category.codes
    else
      []
    end
  end
end
