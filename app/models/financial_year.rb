class FinancialYear < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  has_one :budget
  has_many :payment_vouchers
  has_many :revenues
  acts_as_paranoid

  validates :name,        presence: true
  validates :start_date,  presence: true
  validates :end_date,    presence: true
  validates :is_current,  presence: true


  def self.current
    FinancialYear.find_by(is_current: 'yes')
  end
end
