module SharedTasks
  extend ActiveSupport::Concern

  private

  def add_zeros(number)
    if number.to_s.size == 1
      "000#{number}"
    elsif number.to_s.size == 2
      "00#{number}"
    elsif number.to_s.size == 3
      "0#{number}"
    else
      number.to_s
    end
  end

  def removeCommas
    self.amount = self.amount.gsub(",","")
  end
end
