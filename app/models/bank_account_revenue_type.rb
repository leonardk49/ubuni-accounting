class BankAccountRevenueType < ApplicationRecord
  belongs_to :bank_account
  belongs_to :revenue_type
end
