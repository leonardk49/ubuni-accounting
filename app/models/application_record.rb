class ApplicationRecord < ActiveRecord::Base
  has_many :system_logs, as: :loggable
  self.abstract_class = true
end
