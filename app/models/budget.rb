class Budget < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :financial_year
  has_many :budget_revenues
  has_many :budget_expenditures
end
