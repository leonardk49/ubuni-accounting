class Revenue < ApplicationRecord
  include SharedTasks
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :accounting_code, polymorphic: true

  before_save :removeCommas

  def update_bank_accounts(bank_account_divisions)
    #to do make sure all amounts in the database become float/big decimal
    self.money_divisions = {}
    bank_account_divisions.each do |money_division|
      self.money_divisions[money_division[0]] = money_division[1]
      self.save
      bank_account = BankAccount.find(money_division[0])
      increment = BigDecimal.new(money_division[1])
      bank_account.add_balance(increment)
    end
  end

  def update_budget_revenue
    code = Code.find(self.accounting_code_id)
    #to do ensure that budget expenditure is unique
    budget_revenue = BudgetRevenue.find_by(accounting_code_type: 'Code', accounting_code_id: code.id)

    if budget_revenue
      actual_revenue = budget_revenue.actual

      increment = BigDecimal.new(amount)
      actual_revenue = 0.0 if actual_revenue.nil?
      actual_revenue = BigDecimal.new(actual_revenue, 2)
      actual_revenue += increment
      budget_revenue.update(actual: actual_revenue)
      puts "Debugging"
      puts "#{increment} - #{actual_revenue}"
    end
  end
end
