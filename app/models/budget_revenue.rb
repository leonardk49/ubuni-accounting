class BudgetRevenue < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :budget
  belongs_to :accounting_code, polymorphic: true
end
