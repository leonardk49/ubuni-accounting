class SubCode < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :code
  has_many :budget_revenues, as: :accounting_code, dependent: :delete_all
  has_many :budget_expenditures, as: :accounting_code, dependent: :delete_all
end
