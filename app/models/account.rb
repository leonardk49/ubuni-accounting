class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :timeoutable and :omniauthable
  has_many :system_logs, as: :loggable
  devise :database_authenticatable, :registerable,:lockable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable,
         :authentication_keys => [:email], :password_length => 8..72
end
