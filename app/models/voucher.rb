class Voucher < ApplicationRecord
  include SharedTasks
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  belongs_to :payment, polymorphic: true
  belongs_to :bank_account
  before_save :removeCommas

  def update_bank_account
    #to do make sure all amounts in the database become float/big decimal
    bank_account = self.bank_account
    decrement = BigDecimal.new(amount)
    bank_balance = bank_account.balance
    bank_balance = 0.0 if bank_balance.nil?
    bank_balance = BigDecimal.new(bank_balance, 2)
    bank_balance -= decrement
    bank_account.update(balance: bank_balance)
    puts "Debugging"
    puts "#{decrement} - #{bank_balance}"
  end

  def update_budget_expenditure
    self.code_amount_divisions.each do |code_id, sub_amount|
      sub_code = SubCode.find(code_id)
      #to do ensure that budget expenditure is unique
      budget_expenditure = BudgetExpenditure.find_by(accounting_code_type: 'Code', accounting_code_id: sub_code.code.id)

      if budget_expenditure
        actual_expenditure = budget_expenditure.actual

        increment = BigDecimal.new(sub_amount)
        actual_expenditure = 0.0 if actual_expenditure.nil?
        actual_expenditure = BigDecimal.new(actual_expenditure, 2)
        actual_expenditure += increment
        budget_expenditure.update(actual: actual_expenditure)
        puts "Debugging"
        puts "#{increment} - #{actual_expenditure}"
      end
    end
  end
end
