class PettyCash < ApplicationRecord
  belongs_to :financial_year
  belongs_to :payment_voucher
  has_many :petty_cash_vouchers
end
