class Role < ApplicationRecord
  has_many :system_logs, as: :loggable, dependent: :destroy
  has_paper_trail
  AUTHORIZABLE = ['budget','role','financial_year']
end
