function numberWithCommas(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function allWhiteSpaces(string){
  if ( $.trim(string) == '' ){
    return true;
  }else {
    return false;
  }
}

function removeLeadingZeros(string){
  while (string.charAt(0) == '0') {            // Assume we remove all leading zeros
    if (string.length == 1) { break };       //But not final one.
    if (string.charAt(1) == '.') { break };  //Nor one followed by '.'
    string = string.substr(1, string.length-1)
  }

  return string;
}

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

function toastNotification(type,message){
  clearNotification();
  toastr[type](message);
}

function clearNotification(){
  toastr.remove();
}

function removeCommas(string){
  return string.replace(/\,/g,"");
}

String.prototype.format = function () {
  var args = arguments;
  return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
    if (m == "{{") { return "{"; }
    if (m == "}}") { return "}"; }
    return args[n];
  });
};

/*
js> '{0} {{0}} {{{0}}} {1} {2}'.format(3.14, 'a{2}bc', 'foo');
     3.14 {0} {3.14} a{2}bc foo
*/

function checkAmount(amountInput, amountInputDiv) {
  $(amountInput).keyup(function(e) {
    if (e.keyCode != 13) {
      clearNotification();
    }

    var amount = $(amountInput).val();
    amount = removeCommas(amount);

    if (isNaN(amount)){
      $(amountInputDiv).removeClass('has-success');
      $(amountInputDiv).addClass('has-error');
      toastNotification('error','amount is not a proper number');
    }else{
      $(amountInputDiv).removeClass('has-error');
      $(amountInputDiv).addClass('has-success');
      amount = removeLeadingZeros(numberWithCommas(amount));
      $(amountInput).val(amount);
    }

    if (allWhiteSpaces(amount)){
      $(amountInputDiv).removeClass('has-error');
      $(amountInputDiv).removeClass('has-success');
    }
  });

}
