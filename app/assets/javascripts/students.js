function residenceCheck(value){
  if (value == 'yes'){
    $('.external-resident').hide()
    $('.internal-resident').show()
  }else {
    $('.internal-resident').hide()
    $('.external-resident').show()
  }
}

$(document).ready(function() {
  var value = $("input[name='student[internal_resident]']:checked").val();
  residenceCheck(value);

   $("input[name='student[internal_resident]']").change(function() {
     var value = $("input[type=radio][name='student[internal_resident]']:checked").val();
     residenceCheck(value);
   });


});
