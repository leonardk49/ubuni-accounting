$(document).ready(function() {
  var transactionType = '';
  if ($('#transction-type-div').length > 0){
    transactionType = $('#transction-type-div').data('transaction-type')
  }

  if ($('#transction-type-div').length > 0 && $('#add-{0}-form'.format(transactionType)).length > 0){
    $('#add-{0}-form'.format(transactionType)).submit(function (e) {
        frm = $(this);
        e.preventDefault();

        var selectedCode = $('#add-{0}-form select option:selected'.format(transactionType));
        var codeName = selectedCode.data('name');
        var code = selectedCode.data('code');
        var amount = $('#{0}-amount'.format(transactionType)).val();
        amount = removeCommas(amount);

        if (!allWhiteSpaces(amount) && !isNaN(amount)){
          amount = numberWithCommas(amount);
          var row = '<tr><td>#</td><td>' + code + '</td><td>' + codeName + '</td><td>'+ amount + '</td><td></td></tr>';
          $.ajax({
              type: frm.attr('method'),
              url: frm.attr('action'),
              data: {code: code, amount: amount},
              dataType: "json",
              success: function (data) {
                if (data.success == true ){
                  $('#budget-{0}-rows'.format(transactionType)).append(row);
                  toastNotification('success',data.message);
                }else{
                  toastNotification('error',data.message);
                }

                console.log(data);

              },
              error: function (data) {
                  console.log('An error occurred.');
                  console.log(data);
              },
          });
        }
      });

    $('#{0}-error-icon'.format(transactionType)).hide();
    $('#add-{0}-link'.format(transactionType)).click(function(){
      var amount = $('#{0}-amount'.format(transactionType)).val();
      amount = removeCommas(amount);

      if (isNaN(amount)){
        $('#{0}-amount-div'.format(transactionType)).addClass('has-error');
        toastNotification('error','amount is not a proper number');
      }else {
        $('#add-{0}-form'.format(transactionType)).submit();
      }

    });

    $('#{0}-amount'.format(transactionType)).keyup(function(e) {
      if (e.keyCode != 13) {
        clearNotification();
      }

      $('#{0}-error-icon'.format(transactionType)).show()
      var amount = $('#{0}-amount'.format(transactionType)).val();
      amount = amount.replace(/\,/g,"");

      if (isNaN(amount)){
        $('#{0}-amount-div'.format(transactionType)).addClass('has-error');
        $('#{0}-error-icon'.format(transactionType)).removeClass('ion-checkmark-circled text-success');
        $('#{0}-error-icon'.format(transactionType)).addClass('ion-close-circled text-danger');
        toastNotification('error','amount is not a proper number');
      }else{
        $('#{0}-amount-div'.format(transactionType)).removeClass('has-error');
        $('#{0}-error-icon'.format(transactionType)).removeClass('ion-close-circled text-danger');
        $('#{0}-error-icon'.format(transactionType)).addClass('ion-checkmark-circled text-success');
        amount = removeLeadingZeros(numberWithCommas(amount));
        $('#{0}-amount'.format(transactionType)).val(amount);
      }

      if (allWhiteSpaces(amount)){
        $('#{0}-error-icon'.format(transactionType)).hide()
      }
    });
  }
});
