$(document).ready(function() {
  if ($('#payment-form').length > 0){
    checkAmount('.amount', '.amount-div');
    checkAmount('.code-amount', '.code-amount-div');

    $("#add-payment-code").click(function(){
      var selectedCode = $('#selected-code option:selected').text();
      var codeId = $('#selected-code option:selected').val();
      var elementId = 'selected-code-' + codeId ;

      if ($("#" +elementId).length > 0){
        toastNotification('info', selectedCode + ' already Added');
      }else{
        var amount = $('#code-amount').val();
        amount = removeCommas(amount);

        if (isNaN(amount) || allWhiteSpaces(amount)){
          toastNotification('error', 'Incorrect value');
        }else{
          var codeIdString = $("#selected_codes").val();
          var codeIdArray = codeIdString.split(',');

          var codeAmountsString = $("#selected_codes_amounts").val();
          var codeAmountsArray = codeAmountsString.split(',');

          if (codeIdString == ""){
            codeIdArray = [];
            codeAmountsArray = [];
          }

          codeIdArray.push(codeId);
          codeAmountsArray.push(amount);

          $("#selected_codes").val(codeIdArray.join());
          $("#selected_codes_amounts").val(codeAmountsArray.join());

          var convertedAmounts = codeAmountsArray.map(function(item) {
              return Decimal(item);
          });

          var totalAmounts = Decimal(0);

          for (var i = 0; i < convertedAmounts.length; i++) {
              totalAmounts = totalAmounts.plus(convertedAmounts[i]);
          }

          $('.payment-voucher-codes tr:last').remove() //remove the total row

          $(".payment-voucher-codes").append(
            "<tr><td class='code' id='" + elementId + "'>"+ selectedCode + "</td>" +
            "<td class='code-amount pull-right'>" + numberWithCommas(amount) + "</td>" +
            "<td><a href='javascript:;' class='remove-code' data-code-id='" + codeId + "'><i class='ion-minus-circled' style='font-size: 14px; margin-left: 15px; color: red'></i></a></td></tr>");

            $(".payment-voucher-codes").append(
              "<tr><td><strong>TOTAL</strong></td><td class='total-payment pull-right'><strong>" + numberWithCommas(totalAmounts) + "</strong></td><td></td></tr>")

          $('#amount').val(totalAmounts);
          $('#code-amount').val(null);
          $('#code-amount').closest('.form-group').removeClass('has-success');
        }
      }
    });

    $("table").on('click', '.remove-code', function(){
      var codeId =  $(this).data('code-id');
      codeId = String(codeId);
      var code = 'selected-code-' + codeId;
      var codeIdString = $("#selected_codes").val();
      var codeIdArray = codeIdString.split(',');

      var codeAmountsString = $("#selected_codes_amounts").val();
      var codeAmountsArray = codeAmountsString.split(',');

      var index = codeIdArray.indexOf(codeId);
      codeIdArray.splice(index, 1); //remove the id
      codeAmountsArray.splice(index, 1)

      $("#selected_codes").val(codeIdArray.join());
      $("#selected_codes_amounts").val(codeAmountsArray.join());

      toastNotification('info', 'Removed Code: ' + $("#" + code).text());
      $(this).closest("tr").remove();

      //re calculate total
      var convertedAmounts = codeAmountsArray.map(function(item) {
          return Decimal(item);
      });

      var totalAmounts = Decimal(0);

      for (var i = 0; i < convertedAmounts.length; i++) {
          totalAmounts = totalAmounts.plus(convertedAmounts[i]);
      }

      $('.payment-voucher-codes tr:last').remove() //remove the total row

      $(".payment-voucher-codes").append(
        "<tr><td><strong>TOTAL</strong></td><td class='total-payment pull-right'><strong>" + numberWithCommas(totalAmounts) + "</strong></td><td></td></tr>")

      $('#amount').val(totalAmounts);
      $('#code-amount').val(null);
      $('#code-amount').closest('.form-group').removeClass('has-success');
    });
  }
});
