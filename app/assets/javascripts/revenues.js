function displayRevenueDivisions(){
  var selectedRevenueType = $('#add-revenue-form select option:selected');
  var bankIds = selectedRevenueType.data('bank-ids');
  var percentDivs = selectedRevenueType.data('bank-percentages');
  var percentDivsArray = percentDivs.toString().split(",");

  var bankIdsArray = bankIds.toString().split(",");
  var arrayLength = bankIdsArray.length;
  var amount = $('#revenue-amount').val();
  amount = removeCommas(amount);

  if (isNaN(amount)){
    $('.money-divisions').empty();
  }else {
    if (allWhiteSpaces(amount)){
      $('.money-divisions').empty();
    }else{

      var amountArray = [];

      amount = Decimal(amount);
      for (var i = 0; i < arrayLength; i++){
        var moneyDiv = Number(amount.times(percentDivsArray[i]).times(0.01));
        amountArray.push(moneyDiv);
        // var valueToDisplay = numberWithCommas(Number(amount.times(percentDivsArray[i]).times(0.01)));
        // $("#bank-division-{0}".format(bankIdsArray[i])).text(valueToDisplay);
      }

      var highestValue = Math.max.apply(null, amountArray);
      var indexOfHighest = amountArray.indexOf(highestValue);

      for (var i = 0; i < arrayLength; i++) {
        if (i == indexOfHighest){
          continue;
        }

        var val = amountArray[i];
        var sliced = val.toString().slice(-2);
        if (Number(sliced) % 50 != 0){
          console.log(amountArray[indexOfHighest]);
          amountArray[indexOfHighest] = Number(amountArray[indexOfHighest]) + Number(sliced);
          amountArray[i] = Number(amountArray[i]) - Number(sliced);
          console.log(amountArray[indexOfHighest]);
          console.log(amountArray);
        }
      }

      for (var i = 0; i < arrayLength; i++) {
        var valueToDisplay = Number(amountArray[i]);
        $("#revenue-{0}".format(bankIdsArray[i])).val(valueToDisplay);
        $("#bank-division-{0}".format(bankIdsArray[i])).text(numberWithCommas(valueToDisplay));
      }
    }
  }
}


$(document).ready(function() {
  if ($('#add-revenue-form').length > 0){

    $('#revenue-types').change(function(){
      displayRevenueDivisions()
    });

    $('#revenue-amount').keyup(function(e) {
      displayRevenueDivisions()
    });

    checkAmount('.amount', '.amount-div');
  }
});
