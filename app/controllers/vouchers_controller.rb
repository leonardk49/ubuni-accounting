class VouchersController < ApplicationController
  include Printable
  before_action :set_paper_trail_whodunnit

  def show
    if params[:type] == 'PettyCash'
      @voucher_type = PettyCashVoucher.find(params[:id])
    else
      @voucher_type = PaymentVoucher.find(params[:id])
    end

    print_voucher(@voucher_type)
  end
end
