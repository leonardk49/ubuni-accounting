class BankAccountsController < ApplicationController
  before_action :set_paper_trail_whodunnit

  def new
    @bank_account = BankAccount.new
  end

  def create
    bank_account = BankAccount.new(bank_account_params)
    if bank_account.save
      #to be implemented
      Code.revenue_codes.each do |code|
        code.revenue_division.percentage_divisions["#{bank_account.id}"] = "0"
        code.revenue_division.save
      end
      notes = "Added a Bank Account with name: #{bank_account.name} and id: #{bank_account.id}"
      create_log('create', bank_account, notes)
    else
      #to be implemented
      puts bank_account.errors.full_messages.to_sentence
    end

    respond_to do |format|
      format.html { redirect_to bank_accounts_path}
    end
  end

  def edit
    @bank_account = BankAccount.find(params[:id])
  end

  def update
    bank_account = BankAccount.find(params[:id])
    increment = params[:increment].gsub(",","")
    bank_account.add_balance(increment)
    flash[:success] = "Successfully added balance"
    notes = "Added a Balance of #{increment} to Bank Account : #{bank_account.name} and id: #{bank_account.id}"
    create_log('create', bank_account, notes)

    respond_to do |format|
      format.html { redirect_to bank_accounts_path}
    end
  end

  def index
    @bank_accounts = BankAccount.all
  end

  private

  def bank_account_params
    params.require(:bank_account).permit(:name, :number)
  end
end
