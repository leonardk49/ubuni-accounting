class CodesController < ApplicationController

  def new
    @code = Code.new
    @code_category = CodeCategory.find(params[:code_category_id])
  end

  def create
    code = Code.new(code_params)
    if code.save
      if params[:revenue_divisions].present?
        code.save_revenue_divisions(params[:revenue_divisions])
      end
      notes = "Added a Code with name: #{code.name} and id: #{code.id}"
      create_log('create', code, notes)
    else
      #to be implemented
      puts code.errors.full_messages.to_sentence
    end

    respond_to do |format|
      format.html { redirect_to code_category_path(params[:code_category_id])}
    end
  end

  def show
    @code = Code.find(params[:id])
  end

  private

  def code_params
    params.require(:code).permit(:name, :number).merge(code_category_id: params[:code_category_id])
  end
end
