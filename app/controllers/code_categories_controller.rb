class CodeCategoriesController < ApplicationController

  def new
    @code_category = CodeCategory.new
  end

  def create
    code_category = CodeCategory.new(code_category_params)
    if code_category.save
      #to be implemented
      notes = "Added a Code Category with name: #{code_category.name} and id: #{code_category.id}"
      create_log('create', code_category, notes)
    else
      #to be implemented
      puts code_category.errors.full_messages.to_sentence
    end

    respond_to do |format|
      format.html { redirect_to code_categories_path}
    end
  end

  def show
    @code_category = CodeCategory.find(params[:id])
  end

  def index
    @code_categories = CodeCategory.all
  end

  private

  def code_category_params
    params.require(:code_category).permit(:name, :number)
  end
end
