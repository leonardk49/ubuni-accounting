class PettyCashVouchersController < ApplicationController
  include Printable
  before_action :set_paper_trail_whodunnit

  def new
    @petty_cash_voucher = PettyCashVoucher.new
    @codes = CodeCategory.expenses.sub_codes
  end

  def create
    @petty_cash_voucher = PettyCashVoucher.new(petty_cash_voucher_params)
    if @petty_cash_voucher.save
      save_voucher
      flash[:success] = "Successfully created a petty cash voucher (#{@petty_cash_voucher.number})"
      notes = "Added a payment voucher: voucher number #{@petty_cash_voucher.number} and id: #{@petty_cash_voucher.id}"
      create_log('create', @petty_cash_voucher , notes)
      respond_to do |format|
        format.html { redirect_to financial_year_petty_cash_voucher_path(@current_financial_year,@petty_cash_voucher)}
      end
    else
      flash[:error] = "Sorry please try again"
      puts @petty_cash_voucher.errors.full_messages.to_sentence
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def approve
    petty_cash_voucher = PettyCashVoucher.find(params[:id])
    voucher = petty_cash_voucher.voucher
    voucher.update(approved_by: current_account.id, approval_date: Time.now)
    flash[:success] = "Successfully approved"
    notes = "Approved petty cash Voucher #{petty_cash_voucher.number}"
    create_log('create', petty_cash_voucher, notes)
    redirect_to financial_year_petty_cash_voucher_path(@current_financial_year, petty_cash_voucher)
  end

  def authorize
    petty_cash_voucher = PettyCashVoucher.find(params[:id])
    voucher = petty_cash_voucher.voucher
    voucher.update(authorized_by: current_account.id, authorization_date: Time.now)
    # voucher.update_bank_account
    voucher.update_budget_expenditure
    flash[:success] = "Successfully authorized"
    notes = "Authorized petty cash Voucher #{petty_cash_voucher.number}"
    create_log('create', petty_cash_voucher, notes)
    redirect_to financial_year_petty_cash_voucher_path(@current_financial_year, petty_cash_voucher)
  end

  def index
    @petty_cash_vouchers = PettyCashVoucher.all.where(financial_year_id: @current_financial_year.id)
  end

  def show
    @petty_cash_voucher = PettyCashVoucher.find(params[:id])
    @voucher = @petty_cash_voucher.voucher
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "file_name",  :template => 'admin/petty_cash/invoice.html.erb'   # Excluding ".pdf" extension.
      end
    end
  end

  def print
    @petty_cash_voucher = PettyCashVoucher.find(params[:id])
    @voucher = @petty_cash_voucher.voucher
    print_voucher(@petty_cash_voucher)
  end

  private

  def petty_cash_voucher_params
    #TO DO ensure that petty cash is always done before using the lastest
    petty_cash = PettyCash.order(created_at: :desc).first
    params.require(:petty_cash_voucher).permit(:financial_year_id).merge(petty_cash_id: petty_cash.id)
  end

  def save_voucher
    code_amount_divisions = {}
    code_ids = params[:selected_codes].split(',')
    code_amounts = params[:selected_codes_amounts].split(',')
    code_ids.each do |code_id|
      index = code_ids.index(code_id)
      code_amount_divisions[code_id] = code_amounts[index]
    end
    voucher = Voucher.create(payee: params[:payee], amount: params[:amount],
      amount_in_words: params[:amount_in_words], details: params[:details],
      code_amount_divisions: code_amount_divisions, accounting_code_type: 'SubCode',
    created_by: current_account.id, payment_type: 'PettyCashVoucher', cheque_no: @petty_cash_voucher.petty_cash.payment_voucher.voucher.cheque_no,
    payment_id: @petty_cash_voucher.id, bank_account_id: @petty_cash_voucher.petty_cash.payment_voucher.voucher.bank_account_id)
  end
end
