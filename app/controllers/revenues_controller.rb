class RevenuesController < ApplicationController
  before_action :set_paper_trail_whodunnit

  def new
    @revenue = Revenue.new
    set_codes
  end

  def create
    @revenue = Revenue.new(revenue_params)

    if @revenue.save
      @revenue.update_bank_accounts(bank_account_divisions)
      @revenue.update_budget_revenue
      flash[:success] = "Successfully added a revenue"
      notes = "Added a revenue with amount: #{@revenue.amount} and id: #{@revenue.id}"
      create_log('create', @revenue, notes)
      respond_to do |format|
        format.html { redirect_to financial_year_revenues_path(@current_financial_year)}
      end
    else
      flash[:error] = "Sorry please try again"
      set_codes
      puts @revenue.errors.full_messages.to_sentence
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def index
    @revenues = Revenue.all
  end

  private

  def revenue_params
    params.require(:revenue).permit(:amount, :financial_year_id, :accounting_code_id, :accounting_code_type)
  end

  def bank_account_divisions
    account_money_div = []
    BankAccount.all.each do |bank_account|
      account_money_div << [bank_account.id.to_s, params[bank_account.name.gsub(" ","_").to_sym]]
    end
    account_money_div
  end

  def set_codes
    budget = @current_financial_year.budget
    code_ids = budget.budget_revenues.where(:accounting_code_type => 'Code').pluck(:accounting_code_id) if budget
    # @codes = CodeCategory.revenue.codes.where(id: code_ids)
    @codes = CodeCategory.revenue.codes
  end
end
