module Printable
  extend ActiveSupport::Concern

  private
  def print_voucher(payment_voucher)
    payment_voucher.class.to_s == 'PaymentVoucher' ? class_type = 'PAYMENT VOUCHER' : class_type = 'PETTY CASH VOUCHER'
    payment_voucher.class.to_s == 'PaymentVoucher' ? voucher_number = 'PV' : voucher_number = 'PCV'
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Prawn::Document.new

        print_header(pdf,class_type)

        y = pdf.cursor
        pdf.formatted_text_box [{text: "#{voucher_number}. No: ", styles: [:bold]}, {text: "#{payment_voucher.number}" , styles: [:underline], color: '4169E1'}], at: [0, y]
        pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "#{Time.now.strftime("%d/%m/%Y")}", styles: [:underline]}], at: [440,y]

        if class_type == 'PAYMENT VOUCHER'
          print_pv(pdf,payment_voucher)
        else
          print_pcv(pdf,payment_voucher)
        end
      end
    end
  end

  def date_time_formatter(date)
    if date
      date.strftime("%d/%m/%Y %H:%M")
    else
      ''
    end
  end

  def print_header(pdf,title)
    pdf.image open('https://ubuni.nyc3.digitaloceanspaces.com/cct/assets/images/cct_logo.jpg'), at: [0, 700], width: 100, height: 100
    pdf.image open('https://ubuni.nyc3.digitaloceanspaces.com/cct/assets/images/udsm_logo.png'), at: [455, 700], width: 80, height: 100

    pdf.bounding_box([110, 700], :width => 340) do
      pdf.text "CCT CHAPLAINCY", styles: [:bold], size: 20, align: :center
      pdf.text "UNIVERSITY OF DAR ES SALAAM", styles: [:bold], size: 20, align: :center
      pdf.text "P.O.BOX 35161 DAR ES SALAAM", styles: [:bold], size: 18, align: :center
      pdf.move_down 10
      pdf.text "#{title}", styles: [:bold], size: 25, align: :center, color: '4169E1'
      pdf.transparent(0) { pdf.stroke_bounds }
    end
    pdf.move_down 5
    pdf.stroke_horizontal_rule
    pdf.move_down 20
  end

  def print_pv(pdf,payment_voucher)
    pdf.move_down 30

    voucher = payment_voucher.voucher
    pdf.text "<b>Payee:</b> #{voucher.payee}", inline_format: true

    pdf.move_down 10
    pdf.text "<b>Details:</b>", inline_format: true

    pdf.move_down 5
    pdf.text voucher.details

    pdf.move_down 20

    y = pdf.cursor
    pdf.formatted_text_box [{text: "Cheque No: ", styles: [:bold]}, {text: "#{voucher.cheque_no}" , styles: [:underline], color: 'FF0000'}], at: [0, y]

    voucher.amount.length < 10 ? amount_x = 400 : amount_x = 380
    pdf.formatted_text_box [{text: "Amount: ", styles: [:bold]}, {text: "TZS #{helpers.currency_formatter(voucher.amount)}", styles: [:underline]}], at: [amount_x,y]

    pdf.move_down 30


    pdf.text "<b>Amount in Words:</b>", inline_format: true
    pdf.move_down 5
    pdf.text voucher.amount_in_words

    pdf.move_down 30
    formatted_total = helpers.currency_formatter(voucher.amount)
    data = [['CODE','DESCRIPTION','DR','CR']]
    voucher.code_amount_divisions.each do |code_id, code_amount|
      code = SubCode.find(code_id)
      data << [code.number,code.name, helpers.currency_formatter(code_amount),'']
    end
    data << ['', 'BANK','',formatted_total]
    data << ['','','','']
    data << ['','TOTAL',formatted_total,formatted_total]
    pdf.table(data, header: true, width:550, :cell_style => { :align => :right })

    pdf.move_down 30
    y = pdf.cursor
    pdf.formatted_text_box [{text: "Prepared By: ", styles: [:bold]}, {text: "#{helpers.find_account(voucher.created_by)}" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "#{date_time_formatter(voucher.created_at)}", styles: [:underline]}], at: [400,y]
    pdf.move_down 14
    pdf.text "<b>(CCT CHAPLAINCY ACCOUNTANT)</b>", inline_format: true
    pdf.move_down 10

    y = pdf.cursor
    pdf.formatted_text_box [{text: "Approved By: ", styles: [:bold]}, {text: "#{helpers.find_account(voucher.approved_by)}" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "#{date_time_formatter(voucher.approval_date)}", styles: [:underline]}], at: [400,y]
    pdf.move_down 14
    pdf.text "<b>(CCT CHAPLAINCY TREASURER)</b>", inline_format: true
    pdf.move_down 10

    y = pdf.cursor
    pdf.formatted_text_box [{text: "Authorized By: ", styles: [:bold]}, {text: "#{helpers.find_account(voucher.authorized_by)}" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "#{date_time_formatter(voucher.authorization_date)}", styles: [:underline]}], at: [400,y]
    pdf.move_down 14
    pdf.text "<b>(CCT CHAPLAINCY SECRETARY)</b>", inline_format: true
    pdf.move_down 10

    y = pdf.cursor
    pdf.formatted_text_box [{text: "Received By: ", styles: [:bold]}, {text: "" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "", styles: [:underline]}], at: [400,y]


    send_data pdf.render, filename: "#{payment_voucher.number}.pdf",type: "application/pdf", disposition: "inline"
  end

  def print_pcv(pdf,payment_voucher)
    pdf.move_down 30
    voucher = payment_voucher.voucher
    pdf.text "<b>Payee:</b> #{voucher.payee}", inline_format: true

    pdf.move_down 10
    pdf.text "<b>Details:</b>", inline_format: true

    pdf.move_down 5
    pdf.text voucher.details

    pdf.move_down 20

    pdf.text "<b>Amount:</b> TZS #{helpers.currency_formatter(voucher.amount)}", inline_format: true
    pdf.move_down 20

    pdf.text "<b>Amount in Words:</b>", inline_format: true
    pdf.move_down 5
    pdf.text voucher.amount_in_words

    pdf.move_down 30
    formatted_total = helpers.currency_formatter(voucher.amount)
    data = [['CODE','DESCRIPTION','AMOUNT']]
    voucher.code_amount_divisions.each do |code_id, code_amount|
      code = SubCode.find(code_id)
      data << [code.number,'', helpers.currency_formatter(code_amount)]
    end
    data << ['','TOTAL',formatted_total]
    pdf.table(data, header: true, width:550, :cell_style => { :align => :right })

    pdf.move_down 30
    y = pdf.cursor
    pdf.formatted_text_box [{text: "Prepared By: ", styles: [:bold]}, {text: "#{helpers.find_account(voucher.created_by)}" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "#{date_time_formatter(voucher.created_at)}", styles: [:underline]}], at: [400,y]
    pdf.move_down 20

    y = pdf.cursor
    pdf.formatted_text_box [{text: "Received By: ", styles: [:bold]}, {text: "" , styles: [:underline], color: '4169E1'}], at: [0, y]
    pdf.formatted_text_box [{text: "Date: ", styles: [:bold]}, {text: "", styles: [:underline]}], at: [400,y]

    send_data pdf.render, filename: "#{payment_voucher.number}.pdf",type: "application/pdf", disposition: "inline"
  end
end
