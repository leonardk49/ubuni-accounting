module Loggable
  extend ActiveSupport::Concern

  private
  def create_log(action,instance,notes)
    action_time = Time.now
    SystemLog.create(account_id: current_account.id, action: action,
      action_time: action_time, notes: notes, loggable_id: instance.id, loggable_type: instance.class.to_s )    
  end
end
