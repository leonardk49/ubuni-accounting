class ApplicationController < ActionController::Base
  include Loggable
  protect_from_forgery with: :exception
  before_action :authenticate_account!
  before_action :current_financial_year

  private

  def current_financial_year
    @current_financial_year = FinancialYear.current
  end

  def user_for_paper_trail
    account_signed_in? ? current_account.id : 'Public user'  # or whatever
  end
end
