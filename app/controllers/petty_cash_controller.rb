class PettyCashController < ApplicationController
  before_action :set_paper_trail_whodunnit
  
  def index
    @petty_cash = PettyCash.order(created_at: :desc).where(financial_year_id: @current_financial_year.id)
  end

  def new
    @petty_cash = PettyCash.new
  end

  def create
    @petty_cash = PettyCash.new(petty_cash_params)
    if @petty_cash.save
      flash[:success] = "Successfully Re-imbursed Petty Cash"
      notes = "Re-imbursed a petty Cash => id: #{@petty_cash.id}"
      create_log('create', @petty_cash, notes)
      respond_to do |format|
        format.html { redirect_to financial_year_petty_cash_path(@current_financial_year,@petty_cash)}
      end
    else
      flash[:error] = "Sorry please try again"
      puts @petty_cash.errors.full_messages.to_sentence
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def show
    @petty_cash = PettyCash.find(params[:id])
  end

  private

  def petty_cash_params
    params.require(:petty_cash).permit(:financial_year_id, :amount, :amount_in_words, :payment_voucher_id)
  end
end
