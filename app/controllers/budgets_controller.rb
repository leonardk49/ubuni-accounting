class BudgetsController < ApplicationController

  def index
  end

  def show
    @budget = Budget.find(params[:id])
    if params[:item_type].present?
      if params[:item_type] == 'expenditure'
        @budget_objects = @budget.budget_expenditures
        @codes = CodeCategory.expenses.available_codes(@budget, 'expenditure')
      else
        @budget_objects = @budget.budget_revenues
        @codes = CodeCategory.revenue.available_codes(@budget, 'revenue')
      end
      @state = params[:state]
      @transaction_type = params[:item_type]
    end
  end

  def new
    @budget = Budget.new
  end

  def create
    @budget  = Budget.new(budget_params)
    if @budget.save
      redirect_to financial_year_budget_path(@current_financial_year, @budget)
      flash[:success] = "Successfully saved"
    else
      render :new
      flash[:error] = "Failed"
    end
  end

  def save_expenditure
    data = {}
    if !params[:amount].blank?
      budget = @current_financial_year.budget
      amount = params[:amount]
      code = Code.find_by(number: params[:code])
      budget_expenditure = BudgetExpenditure.new(budget_id: budget.id,
        accounting_code_id: code.id, accounting_code_type: code.class.to_s, estimated: amount)
      if budget_expenditure.save
        data[:message] = "Successfully added the expense"
        data[:success] = true
      else
        data[:message] = "Failed to save, please try again"
        data[:success] = false
      end
    else
      data[:message] = "Please fill in the amount"
      data[:success] = false
    end
    respond_to do |format|
      format.json {render :json => data.to_json}
    end
  end

  def save_revenue
    data = {}
    if !params[:amount].blank?
      budget = @current_financial_year.budget
      amount = params[:amount]
      code = Code.find_by(number: params[:code])
      budget_revenue = BudgetRevenue.new(budget_id: budget.id,
        accounting_code_id: code.id, accounting_code_type: code.class.to_s, estimated: amount)
      if budget_revenue.save
        data[:message] = "Successfully added the revenue"
        data[:success] = true
      else
        data[:message] = "Failed to save, please try again"
        data[:success] = false
      end
    else
      data[:message] = "Please fill in the amount"
      data[:success] = false
    end
    respond_to do |format|
      format.json {render :json => data.to_json}
    end
  end

  private

  def budget_params
    params.require(:budget).permit(:name, :financial_year_id)
  end
end
