class PaymentVouchersController < ApplicationController
  include Printable
  before_action :set_paper_trail_whodunnit

  def new
    @payment_voucher = PaymentVoucher.new
    @codes = CodeCategory.expenses.sub_codes
  end

  def create
    @payment_voucher = PaymentVoucher.new(payment_voucher_params)
    if @payment_voucher.save
      save_voucher
      flash[:success] = "Successfully created a payment voucher(#{@payment_voucher.number})"
      notes = "Added a payment voucher: voucher number #{@payment_voucher.number} and id: #{@payment_voucher.id}"
      create_log('create', @payment_voucher, notes)
      respond_to do |format|
        format.html { redirect_to financial_year_payment_voucher_path(@current_financial_year,@payment_voucher)}
      end
    else
      flash[:error] = "Sorry please try again"
      puts @payment_voucher.errors.full_messages.to_sentence
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def approve
    payment_voucher = PaymentVoucher.find(params[:id])
    voucher = payment_voucher.voucher
    voucher.update(approved_by: current_account.id, approval_date: Time.now)
    flash[:success] = "Successfully approved"
    notes = "Approved payment Voucher #{payment_voucher.number}"
    create_log('create', payment_voucher, notes)
    redirect_to financial_year_payment_voucher_path(@current_financial_year, payment_voucher)
  end

  def authorize
    payment_voucher = PaymentVoucher.find(params[:id])
    voucher = payment_voucher.voucher
    voucher.update(authorized_by: current_account.id, authorization_date: Time.now)
    voucher.update_bank_account
    voucher.update_budget_expenditure
    flash[:success] = "Successfully authorized"
    notes = "Approved payment Voucher #{payment_voucher.number}"
    create_log('create', payment_voucher, notes)
    redirect_to financial_year_payment_voucher_path(@current_financial_year, payment_voucher)
  end

  def index
    @payment_vouchers = PaymentVoucher.all.where(financial_year_id: @current_financial_year.id)
  end

  def show
    @payment_voucher = PaymentVoucher.find(params[:id])
    @voucher = @payment_voucher.voucher
  end

  def print
    @payment_voucher = PaymentVoucher.find(params[:id])
    @voucher = @payment_voucher.voucher
    print_voucher(@payment_voucher)
  end

  private

  def payment_voucher_params
    params.require(:payment_voucher).permit(:financial_year_id)
  end

  def save_voucher
    code_amount_divisions = {}
    code_ids = params[:selected_codes].split(',')
    code_amounts = params[:selected_codes_amounts].split(',')
    code_ids.each do |code_id|
      index = code_ids.index(code_id)
      code_amount_divisions[code_id] = code_amounts[index]
    end
    voucher = Voucher.create(payee: params[:payee], amount: params[:amount],
      amount_in_words: params[:amount_in_words], details: params[:details],
      code_amount_divisions: code_amount_divisions, accounting_code_type: 'SubCode',
    created_by: current_account.id, payment_type: 'PaymentVoucher', cheque_no: params[:cheque_no],
    payment_id: @payment_voucher.id, bank_account_id: params[:bank_account_id])
  end
end
