class FinancialYearsController < ApplicationController
  

  def new
    @financial_year = FinancialYear.new
  end

  def create
    financial_year = FinancialYear.new(financial_year_params)
    if financial_year.save
      #to be implemented
      notes = "Added a financial year with name: #{financial_year.name} and id: #{financial_year.id}"
      create_log('create', financial_year, notes)
    else
      #to be implemented
      puts financial_year.errors.full_messages.to_sentence
    end

    respond_to do |format|
      format.html { redirect_to financial_years_path}
    end
  end

  def show
    @financial_year = FinancialYear.find(params[:id])
  end

  def index
    @financial_years = FinancialYear.all
  end

  private

  def financial_year_params
    params.require(:financial_year).permit(:name, :start_date, :end_date, :is_current)
  end
end
