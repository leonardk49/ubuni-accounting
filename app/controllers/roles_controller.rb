class RolesController < ApplicationController

  def new
    @role = Role.new
  end

  def create
    role = Role.new(roles_params)
    if role.save
      #to be implemented
      notes = "Added a role with name: #{role.name} and id: #{role.id}"
      create_log('create', role, notes)
    else
      #to be implemented
    end

    respond_to do |format|
      format.html { redirect_to roles_path}
    end
  end

  def index
    @roles  = Role.all
  end

  private

  def roles_params
    params.require(:role).permit(:name)
  end
end
