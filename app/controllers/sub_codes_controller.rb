class SubCodesController < ApplicationController

  def new
    @sub_code = SubCode.new
    @code = Code.find(params[:code_id])
  end

  def create
    #use @ for error
    sub_code = SubCode.new(sub_code_params)
    if sub_code.save
      #to be implemented
      notes = "Added a Sub Code with name: #{sub_code.name} and id: #{sub_code.id}"
      create_log('create', sub_code, notes)
    else
      #to be implemented
      puts sub_code.errors.full_messages.to_sentence
    end

    respond_to do |format|
      format.html { redirect_to code_category_code_path(params[:code_category_id],params[:code_id])}
    end
  end

  def show
    @code = Code.find(params[:id])
  end

  private

  def sub_code_params
    params.require(:sub_code).permit(:name, :number).merge(code_id: params[:code_id])
  end
end
