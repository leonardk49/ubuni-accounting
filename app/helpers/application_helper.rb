module ApplicationHelper
  def active_tab(name)
    @active == name ?  'active' : ''
  end

  def currency_formatter(number)
    number_with_precision(number, precision: 2, delimiter: ',')
  end

  def find_account(account_id)
    if account_id

      account = Account.find(account_id)
      if account.firstname && account.lastname
        name = account.firstname + " " + account.lastname
        name.titleize
      else
        "User"
      end
    else
      nil
    end
  end

  def date_formatter(date)
    date.strftime("%d-%m-%Y %H:%M:%S")
  end
end
