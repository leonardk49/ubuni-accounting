module StudentsHelper
  def services
    ['Saa 07:00 Kiswahili', 'Saa 10:00 Kiswahili','Saa 10:00 Kingereza']
  end

  def service_select_option
    services_array = []
    services.each do |service|
      services_array << [service, services.index(service)]
    end

    services_array
  end

  def student_sex(sex)
    if sex == 'male'
      'Mume'
    elsif sex == 'female'
      'Mke'
    else
      ''
    end
  end

  def marital_status(status)
    status?(status)
  end

  def holy_communion(status)
    status?(status)
  end

  def status?(status)
    if status == 'yes'
      'Ndio'
    elsif status == 'no'
      'Hapana'
    else
      ''
    end
  end
end
