# README

## How to get this app running

This application has a website and Other systems
### clone the app

```
$ git clone <the url for the app on bitbucket>
```

### Set up

```
$ cd <app root>
$ bundle install
```
### Create a postgresql user with password and permissions for creating a database

### export the postgresql user and password to the .bashrc file
```
$ nano  ~/.bashrc
```
and then append(add these lines at the bottom of the file)

```
export DEV_USER='your postgresql user'
export DEV_PASS='your postgresql user password'
```
save and exit

### create the database

```
$ rails db:create
```

### run the application

```
$ rails s
```

### inorder to see the website side

`http://localhost:3000`

### inorder to see the admin side

`http://localhost:3000/admin/dashboard`

this will require for you to create an account and confirm it in your email
